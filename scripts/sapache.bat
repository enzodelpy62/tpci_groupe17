#!/bin/bash

APACHE_PATH="~/opt/homebrew/bin/httpd"

start_apache() {
    echo "Starting Apache..."
    cd "$APACHE_PATH"
    ./bin/httpd -k start
}

stop_apache() {
    echo "Stopping Apache..."
    cd "$APACHE_PATH"
    ./bin/httpd -k stop
}

case "$1" in
    start)
        start_apache
        ;;
    stop)
        stop_apache
        ;;
    *)
        echo "Usage: $0 {start|stop}"
        exit 1
        ;;
esac